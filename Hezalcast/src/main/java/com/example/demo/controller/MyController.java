package com.example.demo.controller;

import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dao.PersonDAO;
import com.example.demo.entity.College;
import com.example.demo.entity.Person;

/*My Controller class*/
@RestController
public class MyController {
	
	/*Dao class Declaration data*/
	@Autowired
    private PersonDAO personDAO;
	
	/*Account map for true rep*/
	/*testasdghas*/
	/*1234*/
	/*34556*/
	@Autowired
	private Map<Long, Person> accountMap;
	
	/* getAllEmployees details List of Emp*/
	@GetMapping(value = "/v1/getAllEmployees")
	public List<Person> getAllEmployees() {

		List<Person> s=(List<Person>) personDAO.findAll();
		return s;
	}
	/*Get one Emp*/
	@GetMapping(value = "/v1/getEmployeeById/{id}")
	public Person getEmployeeById(@PathVariable Long id) {

		
		Person s = (accountMap.get(id) != null) ? accountMap.get(id)
				: personDAO.findById(id).get();
		
		accountMap.put(id, s);
		return s;
	}
	

	@GetMapping(value = "/v1/getCollegeDetails")
	public College getCollegeDetails() {

		College c = new College();
		c.setCollegeName("Modern college");
		return  c;
	}
	/* getNameOfEmployee sadas*/
	@GetMapping(value = "/v1/getNameOfEmployee")
	public String giveNameOfEmployeeFromMaster() {

		return "Vaibhav1";
		
	}
}
