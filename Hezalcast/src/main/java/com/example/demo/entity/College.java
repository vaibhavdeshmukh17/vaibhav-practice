package com.example.demo.entity;

import java.io.Serializable;
/*entity*/
public class College implements Serializable{
/*collegeName*/
	private String collegeName;
/*getCollegeName*/
	public String getCollegeName() {
		return collegeName;
	}
/*setCollegeName*/
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	
	
}
