DROP TABLE IF EXISTS PERSON;
 
CREATE TABLE PERSON (
  id LONG AUTO_INCREMENT  PRIMARY KEY,
  Full_Name VARCHAR(250) NOT NULL,
  Employee_Id VARCHAR(250) NOT NULL,
  Designation VARCHAR(250) NOT NULL
);
 
INSERT INTO PERSON (Full_Name, Employee_Id, Designation) VALUES
  ('Vaibhav Deshmukh', '11061506', 'SSE'),
  ('Ashish Ranade', '123456 ','SE'),
  ('Sachin Tebdulkar', '67474784','TL');