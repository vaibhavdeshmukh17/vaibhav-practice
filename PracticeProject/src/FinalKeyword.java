import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class FinalKeyword {
	/*We can not modify final Variable here*/
	
	final String myName="Pooja Deshmukh";
	
	final List<String> listOfnum = new ArrayList<>(Arrays.asList("1","2","3","1","2","3","D"));
	
	/*We can not modify final Collection here*/
	/*myNewList */
	List<String> myNewList = new ArrayList<String>();
	/*unModifiableList */
	List<String> unModifiableList = Collections.unmodifiableList(listOfnum);
	List<String> strList = new ArrayList<String>();
	List<Integer> intList = new ArrayList<Integer>();
	List<Double> doubleList = new ArrayList<Double>();
//	ImmutableList<String> iList = ImmutableList.copyOf(listOfString); 
	
	/* new List fo empl */
	List<Employee> empList = new ArrayList<Employee>();
	/*unModifiableList */
	List<Order> orderList ;
	
	
	public FinalKeyword() {
		// TODO Auto-generated constructor stub
		
		listOfnum.add("D");
		listOfnum.add("a");
		listOfnum.add("b");
		listOfnum.add("c");
		listOfnum.add("d");
		listOfnum.add("3");
		listOfnum.add("4");
		
		/*I will not be able to modify any unmodifiableCollection directly , i will get unSupportedOperationException*/
//		unModifiableList.add("E");
		
		/*but as we uses listOfString (modifiable List) List to build our unmodifiableCollection there is no use of our unModifiableList as it will get changes if we modify underlying collection*/
		/* then question is how to ensure that our collection will not modified even underlying collection changes/ modified*/
	}
}
