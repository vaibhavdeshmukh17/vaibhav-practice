
public class UnderstandingFinalKeyword {

	/*UnderstandingFinalKeyword*/
	public static void main(String args[]) {
		FinalKeyword finalKeyword = new FinalKeyword();
		System.out.println("listOfString--");
		finalKeyword.listOfString.stream().forEach((myStr)->System.out.println(""+myStr));
		System.out.println("myNewList----");
		finalKeyword.myNewList.stream().forEach((myStr)->System.out.println(""+myStr));
		
		System.out.println("unModifiableList----");
		finalKeyword.unModifiableList.stream().forEach((myStr)->System.out.println(""+myStr));
	}
	
	
}
