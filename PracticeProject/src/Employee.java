
/*This is my Employee Class */
public class Employee {

	/*Employee details*/
	/*Employee Firstname*/
	private String firstName;
	/*Employee Lastname*/
	private String lastName;
	/*Employee empId*/
	private String empId;
	
	/*Employee dob in DD/MM/YYYY*/
	private String dob;
	
	/*Employee dob*/
	private String salary;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	
	public String getDob() {
		return firstName;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", empId=");
		builder.append(empId);
		builder.append("]");
		return builder.toString();
	}
	public Employee(String firstName, String lastName, String empId) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.empId = empId;
	}
	
	
}
