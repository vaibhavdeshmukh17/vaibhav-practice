package io.javabrains.moviecatalogservice.client;

import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.CatalogResponse;
import io.javabrains.moviecatalogservice.models.Movie;
import io.javabrains.moviecatalogservice.models.UserRating;

@Component
public class CatalogClient {
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private RestTemplate restTemplate;
	
	public UserRating getUserRating(String userId) {
		
		UserRating userRating = restTemplate.getForObject("http://ratings-data-service/ratingsdata/user/"+userId, UserRating.class);
		logger.info("In Movie CatalogClient in getUserRating method userRating = {}",userRating);	
		return userRating;
	}
	
	public CatalogResponse getMovieDetails(UserRating userRating) {
		 CatalogResponse catalogResponse = new  CatalogResponse ();
		
		logger.info("In Catalog Resourse in getCatalog method userRating = {}",userRating);	
        catalogResponse.setLstCatalogItem(userRating.getRatings().stream()
                .map(rating -> {
                    Movie movie = restTemplate.getForObject("http://movie-info-service/movies/" + rating.getMovieId(), Movie.class);
                    return new CatalogItem(movie.getName(), movie.getDescription(), rating.getRating(),movie.getPort());
                })
                .collect(Collectors.toList()));
        logger.info("In Catalog Client catalogResponse = {}",catalogResponse);	
	return catalogResponse;
	}
}
