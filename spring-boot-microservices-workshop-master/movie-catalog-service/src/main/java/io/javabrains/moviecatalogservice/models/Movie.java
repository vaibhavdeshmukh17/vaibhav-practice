package io.javabrains.moviecatalogservice.models;

public class Movie {
    private String movieId;
    private String name;
    private String description;
    private String port;

    public Movie() {

    }

    public Movie(String movieId, String name, String description,String port) {
        this.movieId = movieId;
        this.name = name;
        this.description = description;
        this.port=port;
    }

    public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}

	public String getMovieId() {
        return movieId;
    }

    public void setMovieId(String movieId) {
        this.movieId = movieId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
