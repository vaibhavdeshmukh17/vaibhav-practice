package io.javabrains.moviecatalogservice.models;

import java.io.Serializable;
import java.util.List;

public class CatalogResponse implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1500722028784422502L;
	private List<CatalogItem> lstCatalogItem;
	private String portOfRatingService;
	public List<CatalogItem> getLstCatalogItem() {
		return lstCatalogItem;
	}
	public void setLstCatalogItem(List<CatalogItem> lstCatalogItem) {
		this.lstCatalogItem = lstCatalogItem;
	}
	public String getPortOfRatingService() {
		return portOfRatingService;
	}
	public void setPortOfRatingService(String portOfRatingService) {
		this.portOfRatingService = portOfRatingService;
	}
	
	
	
}
