package io.javabrains.moviecatalogservice.resources;

import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.CatalogResponse;
import io.javabrains.moviecatalogservice.models.Movie;
import io.javabrains.moviecatalogservice.models.Rating;
import io.javabrains.moviecatalogservice.models.UserRating;
import io.javabrains.moviecatalogservice.services.MovieCatalogService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/catalog")
public class CatalogResource {
 
	private Logger logger = LoggerFactory.getLogger(this.getClass());
	
    @Autowired
    private RestTemplate restTemplate;

    
//    @Autowired
//    WebClient.Builder webClientBuilder;
    
    
    @Autowired
    MovieCatalogService movieCatalogService;

    @RequestMapping("/{userId}")
    public CatalogResponse getCatalog(@PathVariable("userId") String userId) {

        UserRating userRating = movieCatalogService.getUserRating(userId);
        		
        CatalogResponse catalogResponse = movieCatalogService.getMovieDetails(userRating);
        
        catalogResponse.setPortOfRatingService(userRating.getPort());
        return catalogResponse;

    }
}

/*
Alternative WebClient way
Movie movie = webClientBuilder.build().get().uri("http://localhost:8082/movies/"+ rating.getMovieId())
.retrieve().bodyToMono(Movie.class).block();
*/