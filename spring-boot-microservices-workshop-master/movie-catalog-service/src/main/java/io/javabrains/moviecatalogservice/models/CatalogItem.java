package io.javabrains.moviecatalogservice.models;

public class CatalogItem {
    private String name;
    private String desc;
    private int rating;
    private String port;

/*constructor*/

    public CatalogItem(String name, String desc, int rating, String port) {
        this.name = name;
        this.desc = desc;
        this.rating = rating;
        this.port=port;
    }

    
    public String getPort() {
		return port;
	}


	public void setPort(String port) {
		this.port = port;
	}


	public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }
}
