package io.javabrains.moviecatalogservice.services;

import java.util.stream.Collectors;

import org.apache.catalina.startup.Catalina;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import io.javabrains.moviecatalogservice.client.CatalogClient;
import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.CatalogResponse;
import io.javabrains.moviecatalogservice.models.Movie;
import io.javabrains.moviecatalogservice.models.UserRating;

@Service
public class MovieCatalogService {
	
	private Logger logger = LoggerFactory.getLogger(this.getClass());

	@Autowired
	private CatalogClient catalogClient;
	public UserRating getUserRating(String userId) {
		
		UserRating userRating = catalogClient.getUserRating(userId);
		logger.info("In Movie catalog service in getUserRating method userRating = {}",userRating);	
		return userRating;
	}
	
	public CatalogResponse getMovieDetails(UserRating userRating) {
		
		
	   logger.info("-----In MovieCatalogService getMovieDetails------");	
       CatalogResponse cr=catalogClient.getMovieDetails(userRating);
       logger.info("In MovieCatalogService CatalogResponse = {}",cr);	
       return cr;
	}
}
