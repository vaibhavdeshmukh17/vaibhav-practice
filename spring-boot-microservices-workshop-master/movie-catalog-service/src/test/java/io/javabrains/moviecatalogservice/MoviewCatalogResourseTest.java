package io.javabrains.moviecatalogservice;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.test.context.junit4.SpringRunner;

import io.javabrains.moviecatalogservice.client.CatalogClient;
import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.CatalogResponse;
import io.javabrains.moviecatalogservice.models.Rating;
import io.javabrains.moviecatalogservice.models.UserRating;
import io.javabrains.moviecatalogservice.services.MovieCatalogService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class MoviewCatalogResourseTest {

	
	@Autowired
	private MovieCatalogService service;
	
	@MockBean
	CatalogClient catalogClient;
	
	
	@Test
	public void getUsersRatingTest() {
		String userId ="foo"; 
		List<Rating> lstRating = new ArrayList<Rating>();
		lstRating.add(new Rating("movie1", 100));
		lstRating.add(new Rating("movie2", 89));
		
		Mockito.when(catalogClient.getUserRating(userId)).thenReturn(new UserRating("foo", lstRating , "8080"));

		assertEquals("foo", service.getUserRating(userId).getUserId());
		assertEquals(2  , service.getUserRating(userId).getRatings().size());
		
		
	}

	@Test
	public void getMovieDetailsTest() {
		String userId ="foo"; 
		List<Rating> lstRating = new ArrayList<Rating>();
		lstRating.add(new Rating("movie1", 100));
		lstRating.add(new Rating("movie2", 89));
		UserRating ur= new UserRating("foo", lstRating , "8080");
		List<CatalogItem> lstCatalogItem = new ArrayList<CatalogItem>();
		lstCatalogItem.add(new CatalogItem("True Friend", "A card shark and his unwillingly-enlisted friends need to make a lot of cash quick after losing a sketchy poker match. To do this they decide to pull a heist on a small-time gang who happen to be operating out of the flat next door.", 3, "8087"));
		CatalogResponse cr = new CatalogResponse();
		cr.setLstCatalogItem(lstCatalogItem);
		cr.setPortOfRatingService("8082");
		
		Mockito.when(catalogClient.getMovieDetails(ur)).thenReturn(cr);
	
		assertEquals(1, service.getMovieDetails(ur).getLstCatalogItem().size());

		assertEquals("True Friend", service.getMovieDetails(ur).getLstCatalogItem().get(0).getName());
		assertEquals(3  , service.getMovieDetails(ur).getLstCatalogItem().get(0).getRating());
		
		
	}
}
