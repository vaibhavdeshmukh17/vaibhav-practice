package io.javabrains.moviecatalogservice;

import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import javax.print.attribute.standard.Media;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MockMvcBuilder;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.netflix.discovery.DiscoveryClient;

import io.javabrains.moviecatalogservice.models.CatalogItem;
import io.javabrains.moviecatalogservice.models.CatalogResponse;
import io.javabrains.moviecatalogservice.models.Rating;
import io.javabrains.moviecatalogservice.models.UserRating;
import io.javabrains.moviecatalogservice.resources.CatalogResource;
import io.javabrains.moviecatalogservice.services.MovieCatalogService;

@RunWith(SpringRunner.class)
@WebMvcTest(controllers = CatalogResource.class)
@TestPropertySource(locations="classpath:application2.properties")
public class CatalogResourceControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	
	@MockBean
	MovieCatalogService movieCatalogService;
	
	UserRating ur;

	 
	@Before
	public void setup(){
	
		ur = Mockito.mock(UserRating.class);
	}
	
	
	@Test
	public void getCatalogTest() throws Exception {
		
		
		String userId ="foo"; 
		List<Rating> lstRating = new ArrayList<Rating>();
		lstRating.add(new Rating("movie1", 100));
		lstRating.add(new Rating("movie2", 89));
		//	UserRating ur= new UserRating("foo", lstRating , "8080");
		List<CatalogItem> lstCatalogItem = new ArrayList<CatalogItem>();
		lstCatalogItem.add(new CatalogItem("True Friend", "A card shark and his unwillingly-enlisted friends need to make a lot of cash quick after losing a sketchy poker match. To do this they decide to pull a heist on a small-time gang who happen to be operating out of the flat next door.", 3, "8087"));
		CatalogResponse cr = new CatalogResponse();
		cr.setLstCatalogItem(lstCatalogItem);
		cr.setPortOfRatingService("8082");
		
		Mockito.when(movieCatalogService.getUserRating(userId)).thenReturn(ur);
		
		Mockito.when(movieCatalogService.getMovieDetails(ur)).thenReturn(cr);
		
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/catalog/foo").accept(MediaType.APPLICATION_JSON)).andReturn();
		System.out.println("----Response------"+mvcResult.getResponse().getStatus());
		System.out.println("----Response------"+mvcResult.getResponse().getContentAsString());
		JSONAssert.assertEquals("{\"lstCatalogItem\":[{\"name\":\"True Friend\",\"desc\":\"A card shark and his unwillingly-enlisted friends need to make a lot of cash quick after losing a sketchy poker match. To do this they decide to pull a heist on a small-time gang who happen to be operating out of the flat next door.\",\"rating\":3,\"port\":\"8087\"}],\"portOfRatingService\":null}", mvcResult.getResponse().getContentAsString(),true);
		Mockito.verify(movieCatalogService).getMovieDetails(ur);
	}
	
}
