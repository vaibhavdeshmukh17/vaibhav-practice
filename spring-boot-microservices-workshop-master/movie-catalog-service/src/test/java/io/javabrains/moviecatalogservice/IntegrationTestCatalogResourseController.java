package io.javabrains.moviecatalogservice;


import static org.junit.Assert.assertEquals;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.skyscreamer.jsonassert.JSONAssert;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.hamcrest.Matchers.*;
//import static org.springframework.test.web.server.request.MockMvcRequestBuilders.*;
//import static org.springframework.test.web.server.result.MockMvcResultMatchers.content;
//import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
//import static org.springframework.test.web.server.result.MockMvcResultMatchers.status;
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,classes = MovieCatalogServiceApplication.class)
@AutoConfigureMockMvc
public class IntegrationTestCatalogResourseController {

	
	@Autowired
	MockMvc mockMvc;
	
	@Test
	public void getCatalogTest() throws Exception {
		
		MvcResult mvcResult = mockMvc.perform(MockMvcRequestBuilders.get("/catalog/foo").accept(MediaType.APPLICATION_JSON)).andReturn();
//		System.out.println("----Response------"+mvcResult.getResponse().getStatus());
		System.out.println("----Response------"+mvcResult.getResponse().getContentAsString());
		JSONAssert.assertEquals("{\"lstCatalogItem\":[{\"name\":\"Lock, Stock and Two Smoking Barrels\",\"desc\":\"A card shark and his unwillingly-enlisted friends need to make a lot of cash quick after losing a sketchy poker match. To do this they decide to pull a heist on a small-time gang who happen to be operating out of the flat next door.\",\"rating\":3,\"port\":\"8082\"},{\"name\":\"Star Trek: Insurrection\",\"desc\":\"When an alien race and factions within Starfleet attempt to take over a planet that has \\\"regenerative\\\" properties, it falls upon Captain Picard and the crew of the Enterprise to defend the planet's people as well as the very ideals upon which the Federation itself was founded.\",\"rating\":4,\"port\":\"8082\"}],\"portOfRatingService\":\"8083\"}\r\n" + 
				"", mvcResult.getResponse().getContentAsString(),true);
//		 mockMvc.perform(MockMvcRequestBuilders.
//			        get("/catalog/foo")).
//			        andExpect(MockMvcResutMatchers.status().isOk()).
//			        andExpect(MockMvcResutMatchers.content().string("{\"id\":1, "flavor":"Butterscotch"}"));
		
//		 mockMvc.perform(MockMvcRequestBuilders.get("/catalog/foo")
//	        )
//	                .andExpect(status().isOk())
//	                .andExpect(content().mimeType(IntegrationTestUtil.APPLICATION_JSON_UTF8))
//	                .andExpect(jsonPath("$", hasSize(2)))
//	                .andExpect(jsonPath("$[0].id", is(1)))
//	                .andExpect(jsonPath("$[0].description", is("Lorem ipsum")))
//	                .andExpect(jsonPath("$[0].title", is("Foo")))
//	                .andExpect(jsonPath("$[1].id", is(2)))
//	                .andExpect(jsonPath("$[1].description", is("Lorem ipsum")))
//	                .andExpect(jsonPath("$[1].title", is("Bar")));
//		 
//		 
//		assertEquals("foo", mvcResult.getResponse().get);
	}
}
