package io.javabrains.ratingsdataservice.model;

import java.util.Arrays;
import java.util.List;

public class UserRating {

    private String userId;
    private List<Rating> ratings;
    private String port;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public List<Rating> getRatings() {
        return ratings;
    }

    public void setRatings(List<Rating> ratings) {
        this.ratings = ratings;
    }

    public void initData(String userId) {
        this.setUserId(userId);
        this.setRatings(Arrays.asList(
                new Rating("100", 3),
                new Rating("200", 4)
        ));
    }

	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("UserRating [userId=");
		builder.append(userId);
		builder.append(", ratings=");
		builder.append(ratings);
		
		builder.append(", port=");
		builder.append(port);
		builder.append("]");
		return builder.toString();
	}

	public String getPort() {
		return port;
	}

	public void setPort(String port) {
		this.port = port;
	}
    
    
}
