package com.huaylupo.microservice.product.model;


public class ResourceNotFoundException extends RuntimeException{

	 /**
	 * 
	 */
	private static final long serialVersionUID = -8313891814997591359L;

	public ResourceNotFoundException(String message) {
	        super(message);
	    }
}
