package com.huaylupo.microservice.product.model;

import java.io.Serializable;

public class OwnerAddress implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -9070055187426819051L;
	private String city;
	private String country;
	private String state;
	private String postalCode;
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getPostalCode() {
		return postalCode;
	}
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
	public OwnerAddress(String city, String country, String state, String postalCode) {
		super();
		this.city = city;
		this.country = country;
		this.state = state;
		this.postalCode = postalCode;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("OwnerAddress [city=");
		builder.append(city);
		builder.append(", country=");
		builder.append(country);
		builder.append(", state=");
		builder.append(state);
		builder.append(", postalCode=");
		builder.append(postalCode);
		builder.append("]");
		return builder.toString();
	}
	
	public OwnerAddress() {
		
	}
}
