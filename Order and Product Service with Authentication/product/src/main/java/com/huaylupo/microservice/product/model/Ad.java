package com.huaylupo.microservice.product.model;

import java.io.Serializable;

public class Ad implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 8686433145438774927L;
	private String company;
	private String url;
	private String text;
	public String getCompany() {
		return company;
	}
	public void setCompany(String company) {
		this.company = company;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public String getText() {
		return text;
	}
	public void setText(String text) {
		this.text = text;
	}
	public Ad(String company, String url, String text) {
		super();
		this.company = company;
		this.url = url;
		this.text = text;
	}
	public Ad() {
	}
	
	
}
