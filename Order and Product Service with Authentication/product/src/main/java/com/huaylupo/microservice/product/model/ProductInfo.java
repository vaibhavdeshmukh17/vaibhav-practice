package com.huaylupo.microservice.product.model;

import java.io.Serializable;

public class ProductInfo implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = -546835405641503079L;
	private MyData data;
	private Ad ad;
	public MyData getData() {
		return data;
	}
	public void setData(MyData data) {
		this.data = data;
	}
	public Ad getAd() {
		return ad;
	}
	public void setAd(Ad ad) {
		this.ad = ad;
	}
	public ProductInfo(MyData data, Ad ad) {
		super();
		this.data = data;
		this.ad = ad;
	}
	
	public ProductInfo() {
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductInfo [data=");
		builder.append(data);
		builder.append(", ad=");
		builder.append(ad);
		builder.append("]");
		return builder.toString();
	}
	
}
