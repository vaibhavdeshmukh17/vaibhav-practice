package com.huaylupo.microservice.product.model;

import java.io.Serializable;

public class ProductOwner implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 7787092520316947104L;

	private String firstName;
	private String lastName;
	private String ownerId;
	private OwnerAddress addtess;
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getOwnerId() {
		return ownerId;
	}
	public void setOwnerId(String ownerId) {
		this.ownerId = ownerId;
	}
	public OwnerAddress getAddtess() {
		return addtess;
	}
	public void setAddtess(OwnerAddress addtess) {
		this.addtess = addtess;
	}
	public ProductOwner(String firstName, String lastName, String ownerId, OwnerAddress addtess) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.ownerId = ownerId;
		this.addtess = addtess;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("ProductOwner [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", ownerId=");
		builder.append(ownerId);
		builder.append(", addtess=");
		builder.append(addtess);
		builder.append("]");
		return builder.toString();
	}
	
	
	public ProductOwner() {
		
	}
}
