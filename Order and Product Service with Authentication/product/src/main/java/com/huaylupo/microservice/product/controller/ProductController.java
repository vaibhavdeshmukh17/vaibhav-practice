/**
 * 
 */
package com.huaylupo.microservice.product.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.RestTemplate;

import com.huaylupo.microservice.product.model.OwnerAddress;
import com.huaylupo.microservice.product.model.Product;
import com.huaylupo.microservice.product.model.ProductInfo;
import com.huaylupo.microservice.product.model.ProductOwner;
import com.huaylupo.microservice.product.model.ResourceNotFoundException;

/**
 * @author ihuaylupo
 *
 */
@RestController
@RequestMapping("/product")
public class ProductController {
	private static final Logger logger = LoggerFactory.getLogger(ProductController.class);

	private static final String LOCAL_SERVER_PORT = "local.server.port";
	@Autowired
	private Environment environment;

	@Autowired
	RestTemplate template;

	@RequestMapping(method = GET)
	public ResponseEntity<String> getProduct() {
		return ResponseEntity.ok("Product Controller, Port: " + environment.getProperty(LOCAL_SERVER_PORT));
	}

	@RequestMapping(method = GET, value = "/myProduct")
	public Product getProductDetails() {
		logger.info("In ProductController getProductDetails ... ");
		Product p = new Product(1, "Electroniks", "laptop", 50999.2,
				new ProductOwner("Akash", "Sachdeva", "o121", new OwnerAddress("Delhi", "India", "Delhi", "7464874")));
		return p;
	}

	@RequestMapping(method = GET, value = "/getSpecificProduct/{id}")
	public ProductInfo getProductInfo(@PathVariable String id) {
		logger.info("In ProductController getProductInfo request Id ={} ", id);
		ResponseEntity<ProductInfo> productInfo = null;
		ProductInfo p=null;
		try {
			HttpHeaders headers = new HttpHeaders();
			headers.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
			headers.add("user-agent",
					"Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/54.0.2840.99 Safari/537.36");
			HttpEntity<String> entity = new HttpEntity<String>("parameters", headers);

			 productInfo = template.exchange("https://reqres.in/api/products/" + id,
					HttpMethod.GET, entity, ProductInfo.class);
			 if (productInfo.getStatusCode().is2xxSuccessful()) {
		           p=productInfo.getBody();
		        } else {
		            throw new RuntimeException("it was not possible to retrieve user profile");
		        }
		} catch (HttpClientErrorException e) {
			System.out.println("Hi in HttpClientErrorException ");
		}
		logger.info("In ProductController getProductInfo method response   = {}", p);
		return p;
	}
}
