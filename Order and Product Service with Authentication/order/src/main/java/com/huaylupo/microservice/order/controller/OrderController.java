/**
 * 
 */
package com.huaylupo.microservice.order.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.lang.annotation.Retention;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.retry.annotation.Backoff;
import org.springframework.retry.annotation.Retryable;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.huaylupo.microservice.order.Exception.ResourceNotFoundException;
import com.huaylupo.microservice.order.model.Ad;
import com.huaylupo.microservice.order.model.MyData;
import com.huaylupo.microservice.order.model.Product;
import com.huaylupo.microservice.order.model.ProductInfo;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

/**
 * @author ihuaylupo
 *
 */
@RestController
@RequestMapping("/order")
public class OrderController {

	private static final String LOCAL_SERVER_PORT = "local.server.port";

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private Environment environment;

	@Autowired
	private Map<String, ProductInfo> accountMap;

	private static final Logger logger = LoggerFactory.getLogger(OrderController.class);

	@RequestMapping(method = POST)
	public ResponseEntity<String> getOrder() {
		return ResponseEntity.ok("Order Controller, Port: " + environment.getProperty(LOCAL_SERVER_PORT));
	}

	@RequestMapping(method = GET)
	public ResponseEntity<String> getOrderWithProducts() {
		logger.info("Get Order with products data ... ");
		// We use the restTemplate to call another service in this case the
		// product-service.
		// Remember that we are using the spring.application.name we defined for the
		// product on the
		// application.properties of the product microservice.
		String product = restTemplate.getForObject("http://product-service/product", String.class);
		logger.info("Returning data ... ");
		return ResponseEntity
				.ok("Order Controller, Port: " + environment.getProperty(LOCAL_SERVER_PORT) + " " + product);

	}

	@RequestMapping(method = GET, value = "/testproduct")
	public Product getProductDetails() {
		logger.info("Get Order for productInfo ... ");
		Product product = restTemplate.getForObject("http://product-service/product/myProduct", Product.class);
		logger.info("Returning data ... {}", product);
		return product;

	}

	@RequestMapping(method = GET, value = "/productInfo/{id}")
	@HystrixCommand(fallbackMethod = "fallbackProductInfo", commandProperties = {
			@HystrixProperty(name = "execution.isolation.thread.timeoutInMilliseconds", value = "15000") })
//	@Retryable(maxAttempts=9,value=Exception.class, backoff=@Backoff(delay = 2000))
	public ProductInfo getProductInfo(@PathVariable String id) {
		logger.info("In getProductInfo .Id is {}.. " + id);
		logger.info("try!!!");
		Map<String, String> vars = new HashMap<>();
		vars.put("id", id);
		ProductInfo product;
		if (accountMap.get(id) != null) {
			product = accountMap.get(id);
			logger.info("try!!!");
			logger.info("----------Its Cached Data----------");
		} else {
			logger.info("Its Not Cached Data");
			String url = "http://product-service/product/getSpecificProduct/" + id;
			product = restTemplate.getForObject(url, ProductInfo.class);
			if (product == null) {
				throw new ResourceNotFoundException("Specified resource not found");
			}
			logger.info("Returning data productInfo ... {}", product);
			accountMap.put(id, product);
		}
		return product;

	}

	public ProductInfo fallbackProductInfo(@PathVariable String id) {
		logger.info("In fallbackProductInfo methiod ");
		return new ProductInfo(new MyData("fallbackId", "fallbackname", "2020", "fallbackcpolor", "34234"),
				new Ad("fallbac", "fallbackurl", "fallbacktext"));
	}
}
