package com.huaylupo.microservice.order.model;

import java.io.Serializable;

public class Product implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4563885016485915447L;

	private int productId;
	private String productCatagory;
	private String productName ;
	private double prize;
	private ProductOwner productowner;
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public String getProductCatagory() {
		return productCatagory;
	}
	public void setProductCatagory(String productCatagory) {
		this.productCatagory = productCatagory;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public double getPrize() {
		return prize;
	}
	public void setPrize(double prize) {
		this.prize = prize;
	}
	public ProductOwner getProductowner() {
		return productowner;
	}
	public void setProductowner(ProductOwner productowner) {
		this.productowner = productowner;
	}
	public Product() {
		super();
		
	}
	
	public Product(int productId, String productCatagory, String productName, double prize, ProductOwner productowner) {
		super();
		this.productId = productId;
		this.productCatagory = productCatagory;
		this.productName = productName;
		this.prize = prize;
		this.productowner = productowner;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Product [productId=");
		builder.append(productId);
		builder.append(", productCatagory=");
		builder.append(productCatagory);
		builder.append(", productName=");
		builder.append(productName);
		builder.append(", prize=");
		builder.append(prize);
		builder.append(", productowner=");
		builder.append(productowner);
		builder.append("]");
		return builder.toString();
	}
	
	
	
}
