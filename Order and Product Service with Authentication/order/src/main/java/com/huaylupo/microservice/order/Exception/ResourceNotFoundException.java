package com.huaylupo.microservice.order.Exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException{

	 /**
	 * 
	 */
	private static final long serialVersionUID = -8313891814997591359L;

	public ResourceNotFoundException(String message) {
	        super(message);
	    }
}
