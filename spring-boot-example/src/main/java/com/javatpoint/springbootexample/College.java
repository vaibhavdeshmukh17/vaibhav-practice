package com.javatpoint.springbootexample;

/*College*/
public class College {

	/* attributes of college*/
	/* attributes of college name*/
	/* attributes of college name*/
	private String name;
	
	/* attributes of college Id*/
	
	private String clgId1;
	private String city2;
	private String stat3e;
	private String country4;
	private String pincod5e;
	private String address26;
	private String address37;
	private String address11;
	
	private String clgId6;
	private String city7;
	private String stat88e;
	private String country84;
	private String pincod58e;
	private String address826;
	private String address837;
	private String address811;
	
	
	private String clgId;
	private String city;
	private String state;
	private String country;
	private String pincode;
	private String address2;
	private String address3;
	private String address1;
	
	
	private String clgIdasd;
	private String cityasd;

	private String pincodeadsasda;
	private String address2asdas;
	private String address3asd;
	private String address1asdasdas;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	public String getClgId() {
		return clgId;
	}
	public void setClgId(String clgId) {
		this.clgId = clgId;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress3(String address3) {
		this.address3 = address3;
	}
	public String getAddress3() {
		return address3;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getCountry() {
		return country;
	}
	public void setCountry(String country) {
		this.country = country;
	}
	public String getPincode() {
		return pincode;
	}
	public void setPincode(String pincode) {
		this.pincode = pincode;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("College [name=");
		builder.append(name);
		builder.append(", clgId1=");
		builder.append(clgId1);
		builder.append(", city2=");
		builder.append(city2);
		builder.append(", stat3e=");
		builder.append(stat3e);
		builder.append(", country4=");
		builder.append(country4);
		builder.append(", pincod5e=");
		builder.append(pincod5e);
		builder.append(", address26=");
		builder.append(address26);
		builder.append(", address37=");
		builder.append(address37);
		builder.append(", address11=");
		builder.append(address11);
		builder.append(", clgId=");
		builder.append(clgId);
		builder.append(", city=");
		builder.append(city);
		builder.append(", state=");
		builder.append(state);
		builder.append(", country=");
		builder.append(country);
		builder.append(", pincode=");
		builder.append(pincode);
		builder.append(", address2=");
		builder.append(address2);
		builder.append(", address3=");
		builder.append(address3);
		builder.append(", address1=");
		builder.append(address1);
		builder.append("]");
		return builder.toString();
	}
	
	public College(String name, String id) {
		super();
		this.name = name;
		this.clgId = id;
	}
	public College(String name, String clgId1, String city2, String stat3e, String country4, String pincod5e,
			String address26, String address37, String address11, String clgId, String city, String state,
			String country, String pincode, String address2, String address3, String address1) {
		super();
		this.name = name;
		this.clgId1 = clgId1;
		this.city2 = city2;
		this.stat3e = stat3e;
		this.country4 = country4;
		this.pincod5e = pincod5e;
		this.address26 = address26;
		this.address37 = address37;
		this.address11 = address11;
		this.clgId = clgId;
		this.city = city;
		this.state = state;
		this.country = country;
		this.pincode = pincode;
		this.address2 = address2;
		this.address3 = address3;
		this.address1 = address1;
	}
	
	
}
