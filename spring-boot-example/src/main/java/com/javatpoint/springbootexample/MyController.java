package com.javatpoint.springbootexample;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {

	private static final Logger logger = LogManager.getLogger(MyController.class);

	List<Employee> lstEmployee = new ArrayList<Employee>();
	List<Employee> lstEmployee1 = new ArrayList<Employee>();
	
	List<Employee> lstEmployee2 = new ArrayList<Employee>();
	List<Employee> lstEmployee3 = new ArrayList<Employee>();
	List<Employee> lstEmployee4 = new ArrayList<Employee>();
	
	List<Employee> lstEmployee5 = new ArrayList<Employee>();
	List<Employee> lstEmployee6 = new ArrayList<Employee>();
	
	List<Employee> lstEmployee7 = new ArrayList<Employee>();
	List<Employee> lstEmployee8 = new ArrayList<Employee>();
	List<Employee> lstEmployee9 = new ArrayList<Employee>();
	List<Employee> lstEmployee10 = new ArrayList<Employee>();
	List<Employee> lstEmployee11= new ArrayList<Employee>();
	
	List<Employee> lstEmployee71 = new ArrayList<Employee>();
	List<Employee> lstEmployee81 = new ArrayList<Employee>();
	List<Employee> lstEmployee92 = new ArrayList<Employee>();
	List<Employee> lstEmployee101 = new ArrayList<Employee>();
	List<Employee> lstEmployee1121= new ArrayList<Employee>();
	List<Employee> lstEmployee17 = new ArrayList<Employee>();
	List<Employee> lstEmployee18 = new ArrayList<Employee>();
	List<Employee> lstEmployee19 = new ArrayList<Employee>();
	List<Employee> lstEmployee110 = new ArrayList<Employee>();
	List<Employee> lstEmployee111= new ArrayList<Employee>();
	/* Health check */
	@RequestMapping("/")
	public String healthCheck() {
		System.out.println("returning Ok");
		return "OK";
		
	}
	/* getEmployeeById using specific Id */
	/*GET Call*/
	@GetMapping(value = "/v1/getEmployeeById/{empId}")
	public Employee getSingledddddEmployee6dasdasd(@PathVariable String empId) {

		System.out.println("request "+empId);
		List<Employee> lstEmployee = prepareEmployeeList(null);
		System.out.println("request empId=" + empId);

		Employee e = lstEmployee.stream().filter(emp -> emp.getEmpId().equals(empId)).findAny().orElse(null);
		System.out.println("returning "+e);
		return e;
	}

	/* getEmployeeById using specific Id */
	/*GET Call*/
	@GetMapping(value = "/v1/getEmployeeById/{empId}")
	public Employee getSingleEmployee7asdasdasd(@PathVariable String empId) {

		System.out.println("request "+empId);
		List<Employee> lstEmployee = prepareEmployeeList(null);
		System.out.println("request empId=" + empId);

		Employee e = lstEmployee.stream().filter(emp -> emp.getEmpId().equals(empId)).findAny().orElse(null);
		System.out.println("returning "+e);
		return e;
	}
	/* getEmployeeById using specific Id */
	/*GET Call*/
	@GetMapping(value = "/v1/getEmployeeById/{empId}")
	public Employee getSingleEmployeessss(@PathVariable String empId) {

		System.out.println("request "+empId);
		List<Employee> lstEmployee = prepareEmployeeList(null);
		System.out.println("request empId=" + empId);

		Employee e = lstEmployee.stream().filter(emp -> emp.getEmpId().equals(empId)).findAny().orElse(null);
		System.out.println("returning "+e);
		return e;
	}

	/* getEmployeeById using specific Id */
	/*GET Call*/
	@GetMapping(value = "/v1/getEmployeeById/{empId}")
	public Employee getSingleEmployee100asdasdas(@PathVariable String empId) {

		System.out.println("request "+empId);
		List<Employee> lstEmployee = prepareEmployeeList(null);
		System.out.println("request empId=" + empId);

		Employee e = lstEmployee.stream().filter(emp -> emp.getEmpId().equals(empId)).findAny().orElse(null);
		System.out.println("returning "+e);
		return e;
	}

	/* getEmployeeById using specific Id */
	/*GET Call*/
	@GetMapping(value = "/v1/getEmployeeById/{empId}")
	public Employee getSingleEmployee200000000(@PathVariable String empId) {

		System.out.println("request "+empId);
		List<Employee> lstEmployee = prepareEmployeeList(null);
		System.out.println("request empId=" + empId);

		Employee e = lstEmployee.stream().filter(emp -> emp.getEmpId().equals(empId)).findAny().orElse(null);
		System.out.println("returning "+e);
		return e;
	}
	/*GET Call*/
	/* getAllEmployees */
	@GetMapping(value = "/v1/getAllEmployees")
	public List<Employee> getAllEmployees() {

		List<Employee> lstEmployee = prepareEmployeeListEmpty(null);
		logger.debug("Employee List {}",lstEmployee);
		System.out.println("returning "+lstEmployee);
		return lstEmployee;
	}

	/*private method*/
	private List<Employee> prepareEmployeeListEmpty(Object object) {
		// TODO Auto-generated method stub
		return null;
	}

	/*Post*/
	/* save employee */
	@PostMapping(value = "/v1/saveEmployee")
	private List<Employee> saveEmployee(@RequestBody Employee employeeToSave) {
		System.out.println("In saveEmployee ");
		List<Employee> lstEmployee = prepareEmployeeList(employeeToSave);
		lstEmployee.add(employeeToSave);
		System.out.println("returning "+lstEmployee);
		return lstEmployee;

	}
	/* update employee
	 * /* update employee */
	@PostMapping(value = "/v1/updateEmployee")
	private List<Employee> updateEmployee6(@RequestBody Employee employeeToUpdate) {


		System.out.println("request "+employeeToUpdate);
		System.out.println("In updateEmployee ");
		List<Employee> lstEmployee = prepareEmployeeList(null);

		List<Employee> filteredList = lstEmployee.stream()
				.filter(emp -> !emp.getEmpId().equals(employeeToUpdate.getEmpId())).collect(Collectors.toList());
		filteredList.add(employeeToUpdate);
		System.out.println("filteredList "+filteredList);
		return filteredList;

	} 
	@PostMapping(value = "/v1/updateEmployee")
	private List<Employee> updateEmployee8(@RequestBody Employee employeeToUpdate) {


		System.out.println("request "+employeeToUpdate);
		System.out.println("In updateEmployee ");
		List<Employee> lstEmployee = prepareEmployeeList(null);

		List<Employee> filteredList = lstEmployee.stream()
				.filter(emp -> !emp.getEmpId().equals(employeeToUpdate.getEmpId())).collect(Collectors.toList());
		filteredList.add(employeeToUpdate);
		System.out.println("filteredList "+filteredList);
		return filteredList;

	}
	/* update employee */
	@PostMapping(value = "/v1/updateEmployee")
	private List<Employee> updateEmployee(@RequestBody Employee employeeToUpdate) {


		System.out.println("request "+employeeToUpdate);
		System.out.println("In updateEmployee ");
		List<Employee> lstEmployee = prepareEmployeeList(null);

		List<Employee> filteredList = lstEmployee.stream()
				.filter(emp -> !emp.getEmpId().equals(employeeToUpdate.getEmpId())).collect(Collectors.toList());
		filteredList.add(employeeToUpdate);
		System.out.println("filteredList "+filteredList);
		return filteredList;

	}
	/* update employee */
	@PostMapping(value = "/v1/updateEmployee1")
	private List<Employee> updateEmployee1(@RequestBody Employee employeeToUpdate) {


		System.out.println("request "+employeeToUpdate);
		System.out.println("In updateEmployee ");
		List<Employee> lstEmployee = prepareEmployeeList(null);

		List<Employee> filteredList = lstEmployee.stream()
				.filter(emp -> !emp.getEmpId().equals(employeeToUpdate.getEmpId())).collect(Collectors.toList());
		filteredList.add(employeeToUpdate);
		System.out.println("filteredList "+filteredList);
		return filteredList;

	}

	private List<Employee> prepareEmployeeList(Employee employeeToSave) {

		System.out.println("request "+employeeToSave);

		System.out.println("In prepareEmployeeList ");

			lstEmployee.add(new Employee("Vaibhav", "Deshmukh", "1"));
			lstEmployee.add(new Employee("Amogh", "Deshmukh", "2"));
			lstEmployee.add(new Employee("Parag", "Deshmukh", "3"));
			lstEmployee.add(new Employee("Shankarshan", "Deshmukh", "4"));
			System.out.println("lstEmployee "+lstEmployee);
		return lstEmployee;
	}
 
	/* get All college Name */
	@GetMapping(value = "/v1/getCollegeById/{clgId}")
	public College getSingleCollege(@PathVariable String clgId){
	System.out.println("In getSingleCollege ");
		List<College> lstCollege = prepareCollegeList123();
		System.out.println("request clgId=" + clgId);

		College c= lstCollege.stream().filter(clg -> clg.getClgId().equals(clgId)).findAny().orElse(null);
		System.out.println("c "+c);
		return c;
	}

	private List<College> prepareCollegeList123() {
		List<College> lst = new ArrayList<College>();
			lst.add(new College("ABC College", "1"));
			lst.add(new College("MyCollege", "2"));
			System.out.println("lst "+lst);
		return lst;
	}
	/* get Customername */
	@GetMapping(value = "/v1/getCustomerName")
	public String getCustomerName(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName");

		return "vaibhav";
	}
	/* get Customername */
	@GetMapping(value = "/v1/getCustomerName")
	public String getCustomerName1(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName1");

		return "vaibhav";
	}
	
	/* get Customername */
	@GetMapping(value = "/v1/getCustomerName")
	public String getCustomerName2(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName2");

		return "vaibhav";
	}
	
	/* get Customername */
	@GetMapping(value = "/v1/getCustomerName")
	public String getCustomerName10(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName3");

		return "vaibhav";
	}
	/* get Customername */
	@GetMapping(value = "/v2/getCustomerName1")
	public String getCustomerName11(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName3");

		return "vaibhav";
	}
	/* get Customername */
	@GetMapping(value = "/v1/getCustomerNamexcvcxvxcvxcvcxvxcv")
	public String getCustomerName3(){System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName3");

		return "vaibhav";
	}
	
	/* get Company name */
	
	@GetMapping(value = "/v2/getCustomerName3")
	public String getCustomerDataName3(){

		System.out.println("return "+"vaibhav");

		System.out.println("request clgId= getCustomerName3");

		return "vaibhav";
	}
	
	/* get Company name */
	@GetMapping(value = "/v1/getCustomerName")
	public String getCompanyName4(){

		System.out.println("return "+"vaibhav");
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	/*asjdasjdhasjdhjashdjashdjashdjasdhjashdjashd*/
	
	
		System.out.println("request clgId= getCustomerName3");

		return "vaibhav";
	}
}
