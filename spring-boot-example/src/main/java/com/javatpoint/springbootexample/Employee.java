package com.javatpoint.springbootexample;

/*This is my Employee Class*/
public class Employee {

	/*Employee details*/
	/*Employee Firstname*/
	private String firstName;
	/*Employee Lastname*/
	/*Employee Lastname*/
	private String lastName;
	
	/*Employee empId*/
	/*Employee */
	private String empData;
	
	/*Employee empId*/
	private String empId;
	
	/*Employee dob*/
	private String dobData;
	
	/*Employee dob*/
	private String dob;
	
	/*Employee dob*/
	private String salary;
	
	private String dep;
	private String mone;
	private String mone1;
	private String mone2;
	
	
	public String getFirstName() {
		return firstName;
	}
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	
	public String getDob() {
		return firstName;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	
	
	public String getSalary() {
		return salary;
	}
	public void setSalary(String salary) {
		this.salary = salary;
	}
	
	public String getEmpData() {
		return empData;
	}
	public void setEmpData(String empData) {
		this.empData = empData;
	}
	public String getDobData() {
		return dobData;
	}
	public void setDobData(String dobData) {
		this.dobData = dobData;
	}
	
	
	public String getDep() {
		return dep;
	}
	public void setDep(String dep) {
		this.dep = dep;
	}
	public String getMone() {
		return mone;
	}
	public void setMone(String mone) {
		this.mone = mone;
	}
	@Override
	public String toString() {
		StringBuilder builder = new StringBuilder();
		builder.append("Employee [firstName=");
		builder.append(firstName);
		builder.append(", lastName=");
		builder.append(lastName);
		builder.append(", empData=");
		builder.append(empData);
		builder.append(", empId=");
		builder.append(empId);
		builder.append(", dobData=");
		builder.append(dobData);
		builder.append(", dob=");
		builder.append(dob);
		builder.append(", salary=");
		builder.append(salary);
		builder.append("]");
		return builder.toString();
	}
	public Employee(String firstName, String lastName, String empId) {
		
		this.firstName = firstName;
		this.lastName = lastName;
		this.empId = empId;
		
	}
	public Employee(String firstName, String lastName, String empData, String empId, String dobData, String dob,
			String salary, String dep, String mone) {
		super();
		this.firstName = firstName;
		this.lastName = lastName;
		this.empData = empData;
		this.empId = empId;
		this.dobData = dobData;
		this.dob = dob;
		this.salary = salary;
		this.dep = dep;
		this.mone = mone;
	}
	
}
